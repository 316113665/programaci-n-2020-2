# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 13:25:25 2020

@author: miche
"""

l1 = [15, 23.6, "Una cadena", 'Otra cadena']
l1.append("Fin")
print(l1)
print(l1.count(15))
x = 1
l1.insert(2,"Aqui")
print(l1)
l1.insert(0,"Inicio")
print(l1)
valor=l1.pop()
print(l1)
print(valor)

valor=l1.pop(len(l1)//2)
print(l1)
print(valor)

def swap(x,y):
    l=[y,x]
    return l

[a,b] = swap(10,-90)
print(a)
print(b)
