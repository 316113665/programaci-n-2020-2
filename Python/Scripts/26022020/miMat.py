# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 13:52:44 2020

@author: miche
"""

def miRaiz(a):
    """
    Regresa una aproximación a la raíz cuadrada del número a.
    Se calcula construyendo rectángulos de área a cuyos lados 
    sean cada vez mas parecidos.
    Se considera una áproximación aceptable cuando la diferencia
    de las longitudes de los lados es menor al error de 0.001
    """
    e = 0.001
    b = 1
    h = a
    while abs(b - h)>e:
        b = (b + h)/2
        h = a/b
    return b


def mcd(a, b): #Encabezado de la función
    q =  a // b
    r = a - b*q
    while r != 0:
        a = b; 
        b = r
        q =  a // b
        r = a - b*q
    return b

resultado = mcd(85,25)
print(resultado)

print(mcd(85,25))


def sigULAM(n):
    if n%2 ==0:
        return n/2
    else:
        return 3*n + 1
    
resultado = sigULAM(15)
print(resultado)
print(sigULAM(2))
print(sigULAM(3))

    



   