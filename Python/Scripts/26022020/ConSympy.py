# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 13:50:24 2020

@author: miche
"""

from sympy import symbols, solve
x, a, b, c, d, e = symbols("x a b c d e")

a = (10000+x)*1.041
b = (a+x)*1.041
c = (b+x)*1.041
d = (c+x)*1.041
e = (d+x)*1.041
f = 50000-e
print(solve(f))