# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 13:50:24 2020

@author: miche
"""

def limpiaPantalla():
    """
    Limpia la pnatalla de la terminal.
    Dependiendo del sistema operativo en que se ejecute la función
    se ejecutará el comando correspondiente para borrar la pantalla.
    """
    import platform, os
    if platform.system() == 'Linux':
        os.system('clear')
    elif platform.system() == 'Windows':
        os.system('cls')
    elif platform.system() == 'Darwin':
        os.system('cls')